const { app, config } = require('./server');

describe('start the koa app', () => {
  test('should start the app on port 3001', async () => {
    const server = await app.listen(config.get('port') + 1);
    expect(typeof server).toEqual('object');
    expect(typeof server.close).toEqual('function');
    server.close();
  });
});
