const request = require('supertest');
const Article = require('./model');
const { server } = require('../../server');

const mockArticle = {
  title: 'my first one',
  text: 'lorem ipsum',
  tags: ['one'],
  userId: '5b83584e2716350d54a0aeec',
};

beforeAll(async () => {
  const article = await new Article(mockArticle).save();
  mockArticle.id = article.id;
});

describe('user module test', () => {
  const agent = request.agent(server);

  test('should fetch all the articles with certain tags', async () => {
    const response = await agent.get('/article?tags=one').send(mockArticle);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
  });

  test('should create a new article', async () => {
    const response = await agent.post('/article').send(mockArticle);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
  });

  test('should update an article', async () => {
    const { id } = mockArticle;
    const response = await agent.put(`/article/${id}`).send(mockArticle);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
  });

  test('should delete an article', async () => {
    const { id } = mockArticle;
    const response = await agent.del(`/article/${id}`).send();
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
  });
});
