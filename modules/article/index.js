const Router = require('koa-router');
const debug = require('debug')('module:article');
const Article = require('./model');

module.exports = (opts) => {
  const { path } = Object.assign({
    path: '/article',
  }, opts);

  const router = new Router();

  router.get(path, async (ctx, next) => {
    debug('Fetching article by tags');
    const tags = (ctx.request.query.tags || '').split(',');
    try {
      const articles = await Article.find({ tags: { $in: tags } });
      ctx.ok(articles);
    } catch (error) {
      throw error;
    }
    return next();
  });

  router.post(path, async (ctx, next) => {
    debug('Creating a new article');
    const data = ctx.request.body || {};
    try {
      const article = await new Article(data).save();
      ctx.ok(article);
    } catch (error) {
      throw error;
    }
    return next();
  });

  router.put(`${path}/:id`, async (ctx, next) => {
    const { id } = ctx.params;
    const data = ctx.request.body || {};
    debug('Updating article with id: %s', id);
    try {
      const article = await Article.findOneAndUpdate(
        { _id: id },
        data,
        { new: true },
      );
      ctx.ok(article);
    } catch (error) {
      throw error;
    }
    next();
  });

  router.delete(`${path}/:id`, async (ctx, next) => {
    const { id } = ctx.params;
    debug('Droping article with id: %s', id);
    try {
      await Article.findOneAndRemove({ _id: id });
      ctx.ok('Article deleted');
    } catch (error) {
      throw error;
    }
    next();
  });

  return router.routes();
};
