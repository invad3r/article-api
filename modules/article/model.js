const mongoose = require('mongoose');

const { Schema } = mongoose;
const articleSchema = new Schema({
  userId: {
    type: Schema.ObjectId,
    ref: 'user',
  },
  title: {
    type: String,
  },
  text: {
    type: String,
  },
  tags: [String],
});

module.exports = mongoose.model('article', articleSchema, 'articles');
