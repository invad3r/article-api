const Koa = require('koa');
const mount = require('koa-mount');

function handleMongoError(opts, err) {
  const message = Object.keys(err.errors).map(key => err.errors[key].message);
  return {
    status: 400,
    message: message ? message.join(', ') : 'Bad request',
  };
}

function defaultHandler(opts, err) {
  if (opts.throw) {
    throw err;
  }
  return {
    status: err.statusCode || err.status || 500,
    message: 'test server Error',
  };
}

module.exports = (opts) => {
  const app = new Koa();
  app.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      let error;
      // will only respond with JSON
      switch (err.name) {
        case 'ValidationError':
          error = handleMongoError(opts, err);
          break;
        default:
          error = defaultHandler(opts, err);
      }

      ctx.status = error.status;
      ctx.body = { message: error.message };
    }
  });

  return mount('/', app);
};
