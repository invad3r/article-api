const Koa = require('koa');
const mount = require('koa-mount');

module.exports = () => {
  const app = new Koa();
  app.use(async (ctx, next) => {
    const API_KEY = ctx.headers.api_key;
    if (!API_KEY || API_KEY !== process.env.API_KEY) {
      return ctx.unauthorized({ message: 'Invalid or missing token' });
    }

    return next();
  });

  return mount('/', app);
};
