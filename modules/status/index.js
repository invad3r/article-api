const Router = require('koa-router');

module.exports = (opts) => {
  const options = Object.assign({
    path: '/status',
  }, opts);

  const router = new Router();
  const started = new Date();
  router.get(options.path, async (ctx) => {
    const uptime = new Date().getTime() - started.getTime();
    ctx.ok({ started: started.toISOString(), uptime });
  });
  return router.routes();
};
