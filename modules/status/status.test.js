const request = require('supertest');
const { server } = require('../../server');

describe('server status', () => {
  const agent = request.agent(server);

  test('should respond with 200', async () => {
    const response = await agent.get('/status');
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
  });
});
