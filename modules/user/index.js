const Router = require('koa-router');
const debug = require('debug')('module:user');
const User = require('./model');

module.exports = (opts) => {
  const { path } = Object.assign({
    path: '/user',
  }, opts);

  const router = new Router();

  router.post(path, async (ctx, next) => {
    debug('Creating a new user');
    const data = ctx.request.body || {};
    try {
      const user = await new User(data).save();
      ctx.ok(user);
    } catch (error) {
      throw error;
    }
    return next();
  });

  return router.routes();
};
