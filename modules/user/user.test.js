const request = require('supertest');
const { server } = require('../../server');

describe('user module test', () => {
  const agent = request.agent(server);

  test('should create a new user', async () => {
    const response = await agent.post('/user').send({
      name: 'jhon doe',
      avatar: 'http://some-public-uri.com',
    });
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
  });

  test('should fail creating an invalid user', async () => {
    const response = await agent.post('/user').send({
      avatar: 'http://some-public-uri.com',
    });
    expect(response.status).toEqual(400);
    expect(response.type).toEqual('application/json');
  });
});
