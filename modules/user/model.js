const mongoose = require('mongoose');

const { Schema } = mongoose;
const userSchema = new Schema({
  name: {
    type: String,
    required: [true, 'user name is required'],
  },
  avatar: {
    type: String,
  },
});

module.exports = mongoose.model('user', userSchema, 'users');
