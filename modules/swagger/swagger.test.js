const request = require('supertest');
const { server } = require('../../server');

describe('serve swagger docs', () => {
  const agent = request.agent(server);

  test('should respond with 200', async () => {
    const response = await agent.get('/swagger');
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('text/html');
  });

  test('should respond with 200', async () => {
    const response = await agent.get('/swagger.json');
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
  });
});
