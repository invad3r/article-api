const Koa = require('koa');
const Logger = require('koa-logger');
const Cors = require('@koa/cors');
const BodyParser = require('koa-bodyparser');
const Helmet = require('koa-helmet');
const respond = require('koa-respond');
const mount = require('koa-mount');
const config = require('config');
const debug = require('debug')('server');
const mongoose = require('mongoose');

// Load application middlewares as modules
const status = require('./modules/status');
const errorHanlder = require('./modules/error-handler');
const user = require('./modules/user');
const article = require('./modules/article');
const swagger = require('./modules/swagger');
const authorization = require('./modules/authorization');

// MongoDB connect
const dbOpts = config.get('db');
mongoose.connect(process.env.MONGO_URI || dbOpts.uri, {
  useNewUrlParser: true,
}).catch(() => {
  debug('MongoDB connection problem, exiting application');
  process.exit(0);
});

// Set app constants
const PORT = config.get('port');

// init koa app and load middlewares
const app = new Koa();

if (config.get('logs')) {
  app.use(Logger());
}

app.use(Helmet());
app.use(Cors());
app.use(BodyParser({
  enableTypes: ['json'],
  jsonLimit: '5mb',
  onerror(err, ctx) {
    ctx.throw('body parse error', 422);
  },
}));

app.use(respond());
app.use(errorHanlder(config.get('errorHandler')));
app.use(swagger(config.get('swagger')));
app.use(status());

app.use(user());
app.use(article());

// require authorization on user and article endpoints
config.protectedPaths.forEach((resource) => {
  app.use(mount(resource, authorization()));
});

const server = app.listen(PORT).on('error', (err) => {
  debug(err);
});

debug(`Server running on port ${PORT}`);

module.exports = {
  app,
  mongoose,
  server,
  config,
};
