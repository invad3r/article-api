FROM quay.io/aptible/nodejs:v8.1.x
MAINTAINER Juan M Sanchez.

ENV destDir /src/article-api
RUN mkdir -p ${destDir}
WORKDIR ${destDir}

COPY package.json .
RUN npm install --loglevel error
EXPOSE 3000

COPY . .
CMD [ "npm", "start" ]
