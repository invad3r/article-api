# Articles API
This repo serves a [Koa.js](https://github.com/koajs) REST API with CRUD operations for
the User and Articles models described below.

## Installation
requires __node v8.11.3__ or higher for ES2015 and async function support and __mongo 3.4__ or higher.
```
git clone https://gitlab.com/invad3r/article-api
cd article-api/
npm install
npm start
```

## Run with docker
```
docker-compose up
```

## REST Modules
| Module name        | Description                              | Base path |
| ------------------ | ---------------------------------------- | --------- |
| status             | returns the API status and uptime        | /status   |
| swagger            | provides swagger doc for all endpoints   | /swagger  |
| user               | provides a new user endpoint             | /user     |
| article            | provides CRUD endpoints for articles     | /article  |

## Authorization module
Provides a way to secure REST endpoints through an API_KEY strategy.
The `api_key` header of all the incoming request to a protected endpoint will be
compared with the `API_KEY` environment variable to authorize the request.
To add or remove endpoints use the configuration file as shown in this example.
```
"protectedPaths": [
  "/user",
  "/article"
],
```

## Test
Tests are implemented with [Jest](https://jestjs.io/en/)

```
npm run test
```
